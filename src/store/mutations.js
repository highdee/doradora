export default {
    setNotification(state,data){
        state.notification.type=data.type;
        state.notification.message=data.msg;   
        if(!data.unset){
            setTimeout(()=>{
                state.notification.type=0;
                state.notification.message='';
            },6000);
        }
    },
    setRegisterUrl(state,data){
        state.registerUrl = data
    },
    setGlobalNotification(state,data){
        state.globalNotification.type=data.type;
        state.globalNotification.message=data.msg;   
        if(data.unset){
            setTimeout(()=>{
                state.globalNotification.type=0;
                state.globalNotification.message='';
            },6000);
        }
    },
    setAnnouncement(state, data){
        state.announcement = data
    },
    getUser(state){
        var data=localStorage.getItem('monopoly');    
        data=decodeURIComponent(data); 
        data=JSON.parse(data);
        
        state.user=data;
        state.token=data.token; 

        var result=encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('monopoly',result);
    },
    setUser(state,data){ 
        state.user=data;
        state.token=data.token;
        // console.log(data)
        var result=encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('monopoly',result);
    },
    logout(state){
        window.localStorage.removeItem('monopoly');
        state.user={};
        state.token=null;
        window.location.href="/account/login";
    },
}