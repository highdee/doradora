import axios from 'axios';

export default { 
    handleError(context,error){
        console.log(error)
        if(error.request.status == 422){
            var resp=JSON.parse(error.request.response);
            var err=resp.errors; 
            var msg='';
            for(var item in err){
                msg=err[item][0];
                break; // it picks the first error ; 
            }  
            console.log(msg);
            window.ToasterAlert('error', msg)
            // return msg;
        }else if(error.request.status == 303){
            resp=JSON.parse(error.request.response);
            // context.commit('setNotification',{type:2,msg:resp.error}); 
            window.ToasterAlert('error', resp.error)
        }
        else if(error.request.status == 404){
            resp=JSON.parse(error.request.response);
            msg= "Request not found";
            window.ToasterAlert('error', msg) 
        }
        else if(error.request.status == 400){
            resp=JSON.parse(error.request.response);
            msg= resp.message;
            window.ToasterAlert('error', msg) 
        }
        else if(error.request.status == 401){
            msg="Oops! Authentication error, Please login again";
            window.ToasterAlert('error', msg)
            context.commit('logout');
            // window.location.href="/login";
        } 
        else {
            msg="Oops! server error, Please try again";
            window.ToasterAlert('error', msg)
        }
    },
    post(context , data){  
        return new Promise((resolve,reject)=>{
            axios.post(context.state.endpoint+data.endpoint , data.details,{
                headers: {
                    "Authorization": 'Bearer ' + context.state.token, 
                }
            })
            .then((data)=>{ 
                resolve(data);
            })
            .catch((error)=>{
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
    get(context, endpoint){ 
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+endpoint,{
                headers: {
                    "Authorization": 'Bearer ' + context.state.token,
                }
            })
            .then((data)=>{
                // console.log(data)
                resolve(data)
            })
            .catch((error)=>{
                console.log('error', error.request); 
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
    getUser(context,endpoint){
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+endpoint,{
                headers: {
                    "Authorization": 'Bearer ' + context.state.token,
                }
            })
            .then((data)=>{
                console.log(data);
                if(data.data.status){
                    var result=data.data.user
                    result.token=context.state.token
                    context.commit('setUser',result)
                }
                resolve(data)
            })
            .catch((error)=>{
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },

    getDashboard(context){
        // console.log(context.state)
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+'get-dashboard',{
                headers: {
                    "Authorization": 'Bearer ' + context.state.token,
                }
            })
            .then((data)=>{
                var result = data.data.data
                result.token=context.state.token
                context.commit('setUser',result)
                resolve(data)
            })
            .catch((error)=>{
                context.dispatch('handleError',error)
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
} 