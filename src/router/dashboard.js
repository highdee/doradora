import index from './../views/dashboard/dashboard'
import profile from './../views/dashboard/profile'
import layout from '../views/dashboard/layout/layout'
import transactions from './../views/dashboard/transactions'
import character from '../views/dashboard/choose-character'
import gameA from '../views/dashboard/games/gameA' 
import notifications from './../views/dashboard/notifications'
import withdrawal from './../views/dashboard/withdrawal'
import tree from './../views/dashboard/tree'
import oddEven from '../views/dashboard/games/gameB'
import jackpot from '../views/dashboard/games/gameC'

export default [
    {
        path: 'dashboard',
        component: layout,
        children: [
            { path: '/', component: index, name:'dashboard' },
            { path: '/profile', component: profile, name:'profile' },
            { path: '/transactions', component: transactions, name:'transactions' },
            { path: '/characters', component: character, name:'character' },
            { path: '/monopoly', component: gameA, name:'monopoly' }, 
            { path: '/notifications', component: notifications, name:'notifications' },
            { path: '/withdraw', component: withdrawal, name:'withdrawal' },
            { path: '/tree', component: tree, name:'tree' },
            { path: '/odd-even',component: oddEven},
            { path: '/jackpot',component: jackpot},
        ],
        meta: {
            AuthRequired: true
        }
    },
]