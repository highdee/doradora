import Vue from 'vue'
import VueRouter from 'vue-router'
import IndexRoute from '../views/index'
import dashboardRoute from '@/router/dashboard'
import public_page from '@/router/public'


Vue.use(VueRouter)

const routes = [
  {
		path: '/',
		component: IndexRoute,
		children: [
      ...dashboardRoute,
      ...public_page
		]
	}
  
]

const router = new VueRouter({
  routes,
  mode: 'history',
})

export default router

router.beforeEach((to,from,next)=>{
  if(to.matched.some(record => record.meta.AuthRequired)) {
    if (localStorage.getItem('monopoly') == null) {
      next({
        path: '/login',
        name:'login'
      })
    }
    else{
      next();
    }
  }
  next();
})