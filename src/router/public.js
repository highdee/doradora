import create_account from './../views/public/create-account'
import login from '../views/public/login' 
import forgotPassword from '../views/public/forgot-password'
import resetPassword from '../views/public/reset-password'

export default [
    { path: '/account/register', component: create_account, name: 'create_account' },
    {
        path: '/',
        name: 'Home',
        component: () => import('../views/public/home')
    }, 
    {
        path: '/account/login',
        name: 'login',
        component: login
    },
    {
        path: '/login',
        redirect: 'account/login'
    },
    {
        path: '/forgot-password',
        name: 'forgot-password',
        component: forgotPassword
    },
    {
        path: '/reset-password/:token',
        name: 'reset-password',
        component: resetPassword
    },
]