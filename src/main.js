import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuex from 'vuex'
import store from './store/index'
import notification from './components/notification'
import VueCountryCode from "vue-country-code-select";
import i18n from './lang/index'



Vue.use(VueCountryCode);
 
Vue.use(Vuex);
Vue.component('notification',notification);
Vue.config.productionTip = false
const Bus = new Vue();
export default Bus;
new Vue({
  router,
  render: h => h(App),
  store,
  i18n,
}).$mount('#app')
