// element movement
$(document).ready(function(){

    $('.home').click(function(){
        var offset = $('.section-1').offset();
        $('html').animate({scrollTop : offset.top}, 300);
    });

    $('.Scenarios').click(function(){
        var offset = $('.section-2').offset();
        $('html').animate({scrollTop : offset.top}, 300);
    });
    $('.Migration').click(function(){
        var offset = $('.section-3').offset();
        $('html').animate({scrollTop : offset.top}, 300);
    });
    $('.Howto').click(function(){
        var offset = $('.section-4').offset();
        $('html').animate({scrollTop : offset.top}, 300);
    });

});

// mobile-topbar
$(document).ready(function(){

    $('.open').click(function(){
        $('.mobile-top-bar').addClass('active');
    });
    $('.close').click(function(){
        $('.mobile-top-bar').removeClass('active');
    });

    $('.mobile-top-bar .click').click(function(){
        $('.mobile-top-bar').removeClass('active');
    });

});

// topbar-language-toggle
$(document).ready(function(){

    $('.lng').click(function(){
        $('.lng-2').toggleClass('active');
    });

    $('.lng-2').click(function(){
        $('.lng-box-2').toggleClass('active');
        $('.lng-box-1').toggleClass('active');
    });

});

// number-language-toggle
$(document).ready(function(){

    $('.select').click(function(){
        $('.select-2').toggleClass('active');
        $('.polygon').toggleClass('active');
    });

    $('.select-2').click(function(){
        $('.select-box-2').toggleClass('active');
        $('.select-box-1').toggleClass('active');
    });

});